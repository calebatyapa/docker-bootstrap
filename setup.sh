#!/bin/bash


mkdir -p ~/.docker

new_env=$(cat <<EOF
alias dc=docker-compose
alias dm=docker-machine
EOF)

eval "$new_env"

docker rm -f nginx-proxy 2>/dev/null || true
docker network create --driver=bridge public 2>/dev/null || echo "-- Network not created --"
# docker run \
#   --name nginx-proxy \
#   --detach \
#   --restart=always \
#   --net=public \
#   -p 80:80 \
#   -p 443:443 \
#   -v /var/run/docker.sock:/tmp/docker.sock:ro \
#   jwilder/nginx-proxy

read -t 600 -p "What TLD do you want to use? [test] " dev_tld
if [[ -z "$dev_tld" ]]; then
  dev_tld=test
fi
if [ "$(uname)" == "Darwin" ]; then
  docker_ip="0.0.0.0"
  docker rm -f dnsmasq 2>/dev/null || true
  docker run --restart=always --name dnsmasq -d \
    -p 53:53/tcp \
    -p 53:53/udp \
    --cap-add=NET_ADMIN \
    andyshinn/dnsmasq -A /${dev_tld}/$docker_ip

  sudo mkdir -p /etc/resolver

  if [[ -f /etc/resolver/$dev_tld ]]; then
    sudo rm /etc/resolver/test
  fi

  echo "/etc/resolver/$dev_tld"
  echo "nameserver $docker_ip" | sudo tee /etc/resolver/$dev_tld
fi
if [ "$(uname)" == "Linux" ]; then
  echo "address=/test/0.0.0.0" | sudo tee /etc/NetworkManager/dnsmasq.d/test
fi

echo ""
echo "Finished. You should add the following lines to your shell profile:"
echo ""
echo "${new_env}"
echo ""
